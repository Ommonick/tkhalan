<?php

namespace Codderz;

use Facebook\WebDriver\{
    WebDriverKeys
};

class EmolyzerHelp
{
    public
        $accessToken = '5741c4a17008b7044977324f496413e910455b846d1d6e4343957c058334bfb3b542eacbaa7980ef505de',
        $authButton = '';

    function fbLogin($driver,$login, $pass)
    {
        $this->driver = $driver;
        $this->driver->get('http://localhost:8000/');
        $this->driver->waitForElement('.FbAuth', 10);
        $this->driver->findElementByCss('.FbAuth')->click();
        $this->driver->findElementByCss('.AskPermission')->click();
        $this->driver->findElementByCss('#email')->sendKeys($login);
        $this->driver->findElementByCss('#pass')->sendKeys($pass);
        $this->driver->findElementByCss('#loginbutton')->click();
        //ToDo: Дать приложению привилегии использовать FB аккаунт
    }

    function fbAccept($driver)
{
    $this->driver = $driver;
    //todo обработка появления просьбы добавить приложение в fb
    $this->driver->waitForElement('.layerConfirm', 10);
    if ($this->driver->findElementByCss('.layerConfirm')->isDisplayed()) {
        $this->driver->findElementByCss('.layerConfirm')->sendKeys(WebDriverKeys::RETURN_KEY);

    }
}

    function emoCalcFast($driver) {
        //emo check
        $this->driver = $driver;
//        $this->driver->get('http://localhost:8000');
        $this->driver->waitForElement('.TryFull', 10);
        $this->driver->findElementByCss('.TryFull')->click();
        $this->driver->waitForElement('.Continue', 10);
        $this->driver->findElementByCss('.Continue')->click();
        $this->driver->waitForElementVisible('.ToGame', 20);
        $this->driver->findElementByCss('.ToGame')->click();
    }

    function emoCalcStepByStep($driver) {
        $this->driver = $driver;
        $this->driver->get('http://localhost:8000/flow/remove-posts');
        $this->driver->get('http://localhost:8000/flow');
        $this->driver->waitForElement('.LoadPosts', 10);
        $this->driver->findElementByCss('.LoadPosts')->click();
        $this->driver->waitForElement('.Translate');
        $this->driver->findElementByCss('.Translate')->click();
        $this->driver->waitForElement('.DivideToWords');
        $this->driver->findElementByCss('.DivideToWords')->click();
        $this->driver->findElementByCss('.CalcEmoWords')->click();
        $this->driver->findElementByCss('.CalcEmo')->click();
        $this->driver->waitForElement('.ToBegin');
        expect($this->driver->getPageSource())
            ->toContain('neutral');
    }

    function emoGame($driver) {
        $this->driver = $driver;
        $this->driver->get('http://localhost:8000/game/index');
        $this->driver->waitForElement('.NumberOfFriendsAbleToSuggestEmo', 10);
        $SuggestAble = $this->driver
            ->findElementByCss('.NumberOfFriendsAbleToSuggestEmo')->getText();

        $this->driver->findElementByCss('.GuessFriendsEmo')->click();
        $this->driver->waitForElement('.friend_name', 2);
        $selectors = $this->driver->findElementsByCss('.friend_emo_selector');
        $selectors[0]->sendKeys('negative');

        $this->driver->findElementByCss('.guess')->click();
        expect($this->driver->getPageSource())
            ->toContain('Правильный ответ');
        $this->driver->get('http://localhost:8000/game/index');
        //при сбрасывании ответов приравнивается количество друзей в проекте и количество возможных предположений эмоций
        expect($this->driver->findElementByCss('.NumberOfFriendsAbleToSuggestEmo')->getText())
            ->not->toEqual($SuggestAble);
        $this->driver->findElementByCss('.ResetAnswers')->click();
        expect($this->driver->findElementByCss('.NumberOfFriendsAbleToSuggestEmo')->getText())
            ->toEqual($SuggestAble);
    }

}