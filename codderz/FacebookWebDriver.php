<?php

namespace Codderz;

use Facebook\WebDriver\{
    WebDriver, WebDriverBy
};


class FacebookWebDriver
{
    private
        $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->driver, $method], $arguments);
    }


    public function element($selector)
    {
        return $this->driver->findElement(WebDriverBy::cssSelector($selector));
    }

    public function elements($selector)
    {
        return $this->driver->findElements(WebDriverBy::cssSelector($selector));
    }

    public function isDisplayed($selector)
    {
        return $this->element($selector)->isDisplayed();
    }

    public function click($selector)
    {
        $this->element($selector)->click();

        return $this;
    }

    public function keys($keys, $selector, $index = 0)
    {
        $this->elements($selector)[$index]->sendKeys($keys);

        return $this;
    }

    public function text($selector)
    {
        $this->waitForElement($selector);
        return $this->element($selector)->getText();
    }


    public function get($url)
    {
        $this->driver = $this->driver->get($url);

        return $this;
    }




    private function waitForSync(\Closure $closure, $timeOutSeconds = 2, $stepMilliseconds = 500)
    {
        $second = 0;
        $stepTime = $stepMilliseconds / 1000;
        $stepSeconds = floor($stepMilliseconds / 1000);
        $stepNanoseconds = ($stepTime - $stepSeconds) * 1E9;
        $result = \NULL;

        do {
            try {
                $result = (bool)$closure();
            } catch (\Exception $e) {
                continue;
            } finally {
                echo('.');
                time_nanosleep($stepSeconds, $stepNanoseconds);
            }
            if (\TRUE === $result) return true;
            if (\FALSE === $result) throw new \Exception('waiting stopped by closer');
        } while (($second += $stepTime) < $timeOutSeconds);

        throw new \Exception;

        return $this;
    }

    public function waitForElement($selector, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector) {
                return $this->element($selector);
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed waiting for ' . $selector . '');
        }

        return $this;
    }

    public function waitForVisible($selector, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector) {
                $element = $this->element($selector);
                if ($element->isDisplayed()) {
                    return $element;
                } else {
                    throw new \Exception('Element ' . $selector . ' not found');

                }
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed waiting for ' . $selector . '');
        }

        return $this;
    }

    public function waitForText($selector, $text, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector,$text) {
                $source = $this->text($selector);

                if (preg_match("/$text/i", $source)) {
                    return true;
                } else {
                    throw new \Exception('Element ' . $selector . ' has not such text');

                }
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed waiting ' . $selector . 'to contain text');
        }

        return $this;
    }

    public function waitForRegexp($selector, $regular, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector,$regular) {
                $source = $this->text($selector);

                if (preg_match($regular, $source)) {
                    return true;
                } else {
                    throw new \Exception('Element ' . $selector . ' has not such text');

                }
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed regexping ' . $selector . ' to contain words');
        }

        return $this;
    }


    public function waitForHide($selector, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector) {
                return $this->element($selector)->isDisplayed() == false;
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed waiting for ' . $selector . ' to be hidden');
        }

        return $this;
    }

    public function waitForShow($selector, $timeOutSeconds = 10)
    {
        try {
            $this->waitForSync(function () use ($selector) {
                return $this->element($selector)->isDisplayed() == true;
            }, $timeOutSeconds);
        } catch (\Exception $e) {
            throw new \Exception('failed waiting for ' . $selector . ' to be displayed');
        }

        return $this;
    }
}