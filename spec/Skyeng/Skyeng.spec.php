<?php
/**
 * @autor Egor Ommonick
 */

namespace Spec\Skyeng;

use Codderz\FacebookWebDriver;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Spec\Skyeng\Support\Fixture;
use Spec\Skyeng\Support\Helper;


describe("Emolyzer web tests", function () {

    beforeAll(function () {
        $this->host = 'http://localhost:4444/wd/hub';
        $this->capabilities = [WebDriverCapabilityType::BROWSER_NAME => 'chrome'];
        $this->fixture = new Fixture();
        $this->users = $this->fixture->users();

    });

    beforeEach(function () {
        $this->driver = new FacebookWebDriver(
            RemoteWebDriver::create($this->host, $this->capabilities)
        );
        $this->helper = new Helper($this->driver);
    });

    afterEach(function () {
        if ($this->driver) $this->driver->quit();
    });

    xdescribe("авторизуем одного пользователя", function (){
        $user = (new Fixture())->users()[7];

//            it("Отмена привилегий приложению для пользователя {$user['email']}", function () {
//                $this->helper->fbLogin($this->users[0]);
//                $this->driver->get('http://localhost:8000/flow/remove-user');
//            });

            it("регистрация пользователя {$user['email']} с выдачей доступа от fb аккаунта", function () use ($user){
                $this->helper->fbLogin($user);
//                $this->helper->fbAccept($this->driver); //to pass FB privilegies
                $this->driver->waitForRegexp('.box__title', '/(позитивно|нейтрально|негативно)/i', 30);
                var_dump($this->driver->text('.box__title'));
            });
    });

    describe("авторизуем всех пользователей", function (){
        foreach ((new Fixture())->users() as $key => $user) {
            it("регистрация пользователя {$user['email']}", function () use ($key,$user) {
                $this->helper->fbLogin($user);
                $this->helper->fbAccept($this->driver); //to pass FB privilegies
                $this->driver->waitForRegexp('.box__title', '/(позитивная|нейтральная|негативная)/i', 30);
                var_dump($this->driver->text('.box__title'));
            });
        }

    });

    xdescribe("подчищаем данные всех пользователей в системе", function (){
        foreach ((new Fixture())->users() as $key => $user) {
            it("очистка данных пользователя {$user['email']}", function () use ($key,$user) {
                $this->helper->fbLogin($user);
//                $this->helper->fbAccept($this->driver); //to pass FB privilegies
                $this->driver->waitForRegexp('.box__title', '/(позитивная|нейтральная|негативная)/i', 30);
                var_dump($this->driver->text('.box__title'));
                $this->driver->get($this->fixture->url() . '/remove-posts');
                $this->driver->get($this->fixture->url() . '/remove-user');


            });
        }
    });

});