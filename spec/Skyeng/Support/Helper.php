<?php

namespace Spec\Skyeng\Support;

use Facebook\WebDriver\{
    WebDriverKeys, WebDriverBy
};


class Helper
{


    const
        FAST = 5,
        MEDIUM = 10,
        LONG = 20;

    public function __construct($driver)
    {
        $this->driver = $driver;
        $this->fixture = new Fixture();

    }

    function fbLogin($user)
    {
        $driver = $this->driver;
        $driver
            ->get($this->fixture->url())
            ->waitForElement('.button')
            ->click('.button')
            ->keys($user['email'], '#email')
            ->keys($user['pass'], '#pass')
            ->click('#loginbutton');
    }

    function fbAccept()
    {
        $driver = $this->driver;
        $driver->waitForElement('.layerConfirm');
//        $driver->click('.layerConfirm');
        $this->driver->findElement(WebDriverBy::cssSelector('.layerConfirm'))->sendKeys(WebDriverKeys::RETURN_KEY);
//        $driver->sendKeys('.layerConfirm', WebDriverKeys::RETURN_KEY);
    }

    function emoCalcFast()
    {
        $driver = $this->driver;
        //$driver->get('http://localhost:8000');
        $driver->waitForElement('.TryFull')
            ->click('.TryFull')
            ->waitForElement('.Continue')
            ->click('.Continue')
            ->waitForVisible('.ToGame', Helper::LONG)
            ->click('.ToGame');
    }

    function emoCalcStepByStep()
    {
        $driver = $this->driver;
        $driver->get($this->fixture->url() . '/flow/remove-posts')
            ->get($this->fixture->url() . '/flow')
            ->waitForElement('.LoadPosts')
            ->click('.LoadPosts')
            ->waitForElement('.Translate')
            ->click('.Translate')
            ->waitForElement('.DivideToWords')
            ->click('.DivideToWords')
            ->click('.CalcEmoWords')
            ->click('.CalcEmo')
            ->waitForElement('.ToBegin');

//        expect($driver->getPageSource())
//            ->toContain('neutral');
    }

    function emoGame()
    {
        $driver = $this->driver;
        $driver->get($this->fixture->url() . '/game/index')
            ->waitForElement('.NumberOfFriendsAbleToSuggestEmo');

        $SuggestAble = $driver
            ->text('.NumberOfFriendsAbleToSuggestEmo');

        $driver->click('.GuessFriendsEmo')
            ->waitForElement('.friend_name', Helper::FAST)
            ->keys('negative', '.friend_emo_selector', 0)
            ->click('.guess');

        expect($driver->getPageSource())
            ->toContain('Правильный ответ');
        $driver->get($this->fixture->url() . '/game/index');
        //при сбрасывании ответов приравнивается количество друзей в проекте и количество возможных предположений эмоций
        expect($driver->text('.NumberOfFriendsAbleToSuggestEmo'))
            ->not->toEqual($SuggestAble);
        $driver->click('.ResetAnswers');

        expect($driver->text('.NumberOfFriendsAbleToSuggestEmo'))
            ->toEqual($SuggestAble);
    }

}