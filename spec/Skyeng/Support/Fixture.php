<?php

namespace Spec\Skyeng\Support;


class Fixture
{

    public function users() //for local version
    {
        return [
            0 => ['email' => 'adams_gluqevk_chaiwitz@tfbnw.net', 'pass' => 'qwerty123456'],
            1 => ['email' => 'batman_vimblfx_zamoresen@tfbnw.net', 'pass' => 'qwerty123456'],
            2 => ['email' => 'christopher_ofjxuht_goldmanson@tfbnw.net', 'pass' => 'qwerty123456'],
            3 => ['email' => 'barbara_velowtl_zuckerberg@tfbnw.net', 'pass' => 'qwerty123456'],
            4 => ['email' => 'james_pdnkquh_huiwitz@tfbnw.net', 'pass' => 'qwerty123456'],
            5 => ['email' => 'mark_alvzgng_chaser@tfbnw.net', 'pass' => 'qwerty123456'],
            6 => ['email' => 'mike_gzmsnqe_lausen@tfbnw.net', 'pass' => 'qwerty123456'],
            7 => ['email' => 'sharon_irwvybx_liangescu@tfbnw.net', 'pass' => 'qwerty123456']
            /*
            6 пользователь, нейтраль
            Полезный Полезный Полезный Полезный Полезный
            Победители Победители Победители Победители
            Разносторонние Разносторонние Разносторонние
            наркоман наркоман наркоман наркоман наркоман
            Слепой Слепой Слепой Слепой
            дерьмо дерьмо дерьмо

            7 пользователь, нейтраль
            Полезный Полезный Полезный Полезный Полезный
            Победители Победители Победители Победители
            Разносторонние Разносторонние Разносторонние
            наркоман наркоман наркоман наркоман наркоман
            Слепой Слепой Слепой Слепой
            дерьмо дерьмо дерьмо

            */
        ];
    }

    public function webusers() //for web version
    {
        return [
            0 => ['email' => 'anaconda_podmaeo_alisonsky@tfbnw.net', 'pass' => 'qwerty123456'], //a lot of text pos
            1 => ['email' => 'asoqihucfv_1496041403@tfbnw.net', 'pass' => 'qwerty123456'], //a lot of emoji pos
            2 => ['email' => 'lsphnyacqx_1496041391@tfbnw.net', 'pass' => 'qwerty123456'],
            3 => ['email' => 'xvquebczlv_1496041386@tfbnw.net', 'pass' => 'qwerty123456'],
            4 => ['email' => 'xpptpzptjs_1493997311@tfbnw.net', 'pass' => 'qwerty123456'],
            5 => ['email' => 'yqtqjbjfyr_1493997306@tfbnw.net', 'pass' => 'qwerty123456'],
            6 => ['email' => 'tsxmjtepvs_1493997316@tfbnw.net', 'pass' => 'qwerty123456'],
            7 => ['email' => 'irljehsujb_1493997321@tfbnw.net', 'pass' => 'qwerty123456']
        ];

    }

    public function url()
    {
        return 'http://localhost:8000';
    }

    public function weburl()
    {
        return 'http://emo-test.skyeng.su/';
    }

}










