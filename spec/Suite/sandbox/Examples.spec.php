<?php
/**
 * @autor Egor Ommonick
 */
namespace Testing\Spec\Suite;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverBy;
use Codderz\Awaiter;

require_once 'codderz/waitforsync.php';

//var_dump(realpath ('../'));

xdescribe("Common web tests", function() {
    beforeAll(function() {
        $this->awaiter = new Awaiter($this->driver);
        $this->browser_type = 'firefox';
        $this->host = 'http://localhost:4444/wd/hub';
        $this->capabilities = [WebDriverCapabilityType::BROWSER_NAME =>'firefox'];
    });

    beforeEach(function() {
        $this->driver = RemoteWebDriver::create($this->host, $this->capabilities);
    });

    afterEach(function(){
        if($this->driver != null) { $this->driver->quit(); }
    });

    describe("testing page many times", function() {
        foreach(array_fill(0, 3, []) as $values) {
            it("visits Codderz many times", function() {
                $this->driver->get('http://www.codderz.com');
                $destiny = $this->driver->findElement(WebDriverBy::cssSelector('.brand-tagline'))->getText();
                expect($destiny)->toEqual('Web App Development');
            });
        }
    });

    describe("kindium project testing", function() {

        it("kindium login test", function() {
            $this->driver->get('http://web.dev.kindium.ru/promocodes');
//            $this->awaiter->waitForElement($this->driver, '.auth');
            waitForSync(function(){
                $result = ($this->driver->findElement(WebDriverBy::cssSelector('.auth')));
                if ($result) return true;
            } ,10, 50);

            $this->driver->findElement(WebDriverBy::cssSelector('.auth', 10))->click();

            $inputs = $this->driver->findElements(WebDriverBy::cssSelector(
                '#authorization .normalWrap .input-field input'));
            $inputs[0]->sendKeys('test-moder@kindium.ru');
            $inputs[1]->sendKeys('83DU3aB7w26oj');
            $this->driver->findElement(WebDriverBy::cssSelector('.btnWrap'))->click();
//            $this->awaiter->waitForDisappear($this->driver, '.auth span');

            waitForSync(function(){
                $result = ($this->driver->findElement(WebDriverBy::cssSelector('.auth span'))->isDisplayed());
                if ($result == false) return true;
            }, 10, 50);


            expect($this->driver->findElement(WebDriverBy::cssSelector('.auth span'))->isDisplayed())->toBe(false);
        });
    });
});

describe("tests with api requests", function() {
    beforeAll(function() {
        $this->awaiter = new Awaiter($this->driver);
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'https://luckyd.davaisravnim.ru/api/travel-calc-init'
        ]);
    });

    afterAll(function(){
    });

    it("perform GET request", function() {
        $response = $this->client->request('POST', 'travel-calc-init', [
            'auth' => ['$tuff', 'WeZu4v7tWj8Uv6sEeWfz5h0Q']
        ]);

        var_dump(json_decode($response->getBody()));
        var_dump($response->getStatusCode());
    });
    it("do something else", function() {
    });
});

describe("tests with data provider", function() {
    beforeEach(function() {});
    afterEach(function(){});
    $data = [
        [0, 0, 0],
        [0, 1, 1],
        [1, 0, 1],
        [1, 1, 3]
    ];

    foreach($data as $values) {
        list($a, $b, $expected) = $values;
        it("adds {$a} and {$b} correctly", function () use ($expected, $a, $b) {
            expect($a + $b)->toBe($expected);
        });
    }
});