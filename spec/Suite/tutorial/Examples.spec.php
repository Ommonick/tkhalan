<?php
/**
 * @autor Egor Ommonick
 */
namespace Testing\Spec\Suite;

use Codderz\FacebookWebDriver;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

//var_dump(realpath ('../'));

describe("Common web tests", function() {
    beforeAll(function() {
        $this->host = 'http://localhost:4444/wd/hub';
        $this->capabilities = [WebDriverCapabilityType::BROWSER_NAME =>'chrome'];
        $this->driver = null;
    });

    beforeEach(function() {
        $this->driver = new FacebookWebDriver(
            RemoteWebDriver::create($this->host, $this->capabilities)
        );
    });

    afterEach(function(){
        if ($this->driver) $this->driver->quit();
    });

    describe("testing page many times", function() {

        it("visits Codderz many times", function () {
            foreach (array_fill(0, 5, []) as $values) {
                $this->driver->get('http://www.codderz.com');
                $destiny = $this->driver->findElementByCss('.brand-tagline')->getText();
                expect($destiny)->toEqual('Web App Development');
            }
        });
    });

    xdescribe("redirects check from array", function() {
        $urlListing = [
            ['https://davaisravnim.ru/checkup/' , 'https://davaisravnim.ru/medicinskie-obsledovaniya-check-up/'],
            ['https://davaisravnim.ru/finance/deposits/compare/selection/' , 'https://davaisravnim.ru/vklady/']


        ];
        foreach($urlListing as $checkPair) {
            list($urlGiven, $urlExpected) = $checkPair;

            it("visit link and compare to expectated", function() use ($urlGiven, $urlExpected) {
                $this->driver->get($urlGiven);
                sleep(1);
                expect($this->driver->getCurrentUrl())->toEqual($urlExpected);
            });
        }
    });

});

describe("test api requests", function() {
    beforeAll(function() {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'https://luckyd.davaisravnim.ru/api/travel-calc-init'
        ]);
    });

    afterAll(function(){

    });

    it("perform GET request", function() {
        $response = $this->client->request('POST', 'travel-calc-init', [
            'auth' => ['$tuff', 'WeZu4v7tWj8Uv6sEeWfz5h0Q']
        ]);

        var_dump(json_decode($response->getBody()));
        var_dump($response->getStatusCode());
    });

});


describe("tests with data provider", function() {
    beforeEach(function() {});
    afterEach(function(){});
    $data = [
        [0, 0, 0],
        [0, 1, 1],
        [1, 0, 1],
        [1, 1, 3]
    ];

    foreach($data as $values) {
        list($a, $b, $expected) = $values;
        it("adds {$a} and {$b} correctly", function () use ($expected, $a, $b) {
            expect($a + $b)->toBe($expected);
        });
    }
});


